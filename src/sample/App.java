package sample;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class App {

	public static void main(String[] args) {
		
		List<String> words = new ArrayList<>();
		words.add("saabas");
		words.add("karu");
		words.add("rebane");
		words.add("hunt");
		
		Comparator<String> myComparator1 = new MyStringComparator();
		words.sort(myComparator1);
		
//		System.out.println(words);
		
		List<Animal> animals = new ArrayList<>();
		animals.add(new Animal("Karu", 30));
		animals.add(new Animal("Rebane", 20));
		animals.add(new Animal("Hunt", 5));
		animals.add(new Animal("Karu", 3));
		animals.add(new Animal("Karu", 15));
		
		animals.sort(new Comparator<Animal>() {

			@Override
			public int compare(Animal o1, Animal o2) {
				if (o1.name.compareTo(o2.name) == 0) {
					if (o1.tailLength < o2.tailLength) {
						return -1;
					} else if (o1.tailLength == o2.tailLength) {
						return 0;
					} else {
						return 1;
					}
				} else {
					return o1.name.compareTo(o2.name);
				}
			}
		});

		System.out.println(animals);
		
	}

}
