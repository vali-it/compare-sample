package sample;

public class Animal {

	public String name = null;
	public int tailLength = 0;
	
	public Animal(String name, int tailLength) {
		this.name = name;
		this.tailLength = tailLength;
	}
	
	@Override
	public String toString() {
		return String.format("[Name: %s, Tail Length: %s]", this.name, this.tailLength);
	}
}
